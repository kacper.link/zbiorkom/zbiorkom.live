module.exports = (stops) =>
    stops.map((stop) => ({
        id: stop.stop_id,
        name: stop.stop_name,
        location: [Number(stop.stop_lat), Number(stop.stop_lon)],
        type: stop.location_type,
        parent: stop.parent_station,
        code: stop.stop_code,
        platform: stop.platform_code,
        pkp: stop.stop_PKPPLK,
    }));
