module.exports = {
    convertAgency: require("./agency"),
    convertCalendar: require("./calendar"),
    convertFrequencies: require("./frequencies"),
    convertRoutes: require("./routes"),
    convertShapes: require("./shapes"),
    convertStops: require("./stops"),
    convertStopTimes: require("./stopTimes"),
    convertTrips: require("./trips"),
};
