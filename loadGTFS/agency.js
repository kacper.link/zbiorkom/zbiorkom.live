const moment = require("moment-timezone");

module.exports = (agencies) =>
    agencies.map((agency) => ({
        id: agency.agency_id,
        offset: moment().tz(agency.agency_timezone)._offset * 60000,
    }));
