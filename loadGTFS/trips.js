module.exports = (trips, calendar) =>
    trips
        .filter((trip) => calendar(trip.service_id))
        .map((trip) => ({
            id: trip.trip_id,
            route: trip.route_id,
            headsign: trip.trip_headsign,
            shape: trip.shape_id,
            direction: Number(trip.direction_id),
            shortName: trip.trip_short_name,
            brigade: trip.brigade || trip.brigade_id,
            wheelchair: trip.wheelchair_accessible === "1",
            bikes: trip.bikes_allowed === "1",
        }));
