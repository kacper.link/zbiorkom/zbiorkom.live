const mongoose = require("mongoose");
const fastify = (global.fastify = require("fastify")());
const validateCity = require("./util/validateCity");

fastify.register(require("fastify-socket.io"), {
    pingInterval: 30000,
    pingTimeout: 10000,
    cors: {
        origin: "*",
    },
});

mongoose.connect("mongodb://127.0.0.1:27017/").then(() => {
    console.log("Connected to MongoDB");
    fastify
        .listen({
            host: "127.0.0.1",
            port: 3001,
        })
        .then(() => {
            console.log(
                `Fastify listening on ${fastify.server.address().port}`
            );
            fastify.io.on("connection", async (socket) => {
                let cityQuery = socket.handshake.query.city;
                if (!validateCity(cityQuery)) return socket.disconect(true);
                await socket.join(cityQuery);
                socket.emit("positions", global[cityQuery].positions);
            });
        });

    require("./server")();
    require("./cities/warsaw")();
    require("./cities/szczecin")();
    require("./cities/poznan")();
    require("./cities/adelaide")();
    require("./cities/rome")();
});
