module.exports = (time, midnight = true, nextDay = false) => {
    let [h, m, s] = time.split(":");
    return (
        Number(h) * 3600000 +
        Number(m) * 60000 +
        Number(s) * 1000 +
        (midnight ? new Date().setHours(0, 0 - new Date().getTimezoneOffset(), 0, 0) : 0) +
        (nextDay ? 86400000 : 0)
    );
};
