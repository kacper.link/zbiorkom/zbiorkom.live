const rt = require("gtfs-rt-json-v2");

module.exports = async (url) => {
    let data = await rt(url)
        .then((x) => x.entity)
        .catch(() => []);

    return data.map((alert) => ({
        id: alert.id,
        title: alert.alert.headerText.translation[0].text,
        description: alert.alert.descriptionText.translation[0].text,
        url: alert.alert.url?.translation[0].text,
        routes: alert.alert.informedEntity
            ?.map((x) => x.routeId)
            ?.filter((x) => x),
        start: alert.alert.activePeriod?.[0]?.start?.low * 1000 || undefined,
        end: alert.alert.activePeriod?.[0]?.end?.low * 1000 || undefined,
        impediment:
            alert.alert.effect === 1 ||
            alert.alert.effect === 2 ||
            alert.alert.effect === 3 ||
            alert.alert.effect === 4 ||
            alert.alert.effect === 6,
    }));
};
