const { lineString, point, nearestPointOnLine } = require("@turf/turf");

module.exports = ({ vehicle, trip }) => {
    let { properties } = nearestPointOnLine(
        lineString(trip.shapes),
        point(vehicle.location),
        { units: "meters" }
    );
    let stops = trip.stops.map((stop) => ({
        ...stop,
        metersToStop: stop.distance - properties.location,
    }));

    let tripStart = Date.now() - stops[0].arrival;
    let serving = stops.find(
        (stop) => stop.metersToStop < 50 && stop.metersToStop > -50
    );
    let servingIndex = stops.findIndex(
        (stop) => stop.metersToStop < 50 && stop.metersToStop > -50
    );
    let nextStop =
        stops.find((stop) => stop?.metersToStop > 50) ||
        stops[stops.length - 1];
    let nextStopIndex =
        stops.findIndex((stop) => stop?.metersToStop > 50) || stops.length - 1;
    let snIndex =
        tripStart > 0
            ? servingIndex === -1
                ? nextStopIndex
                : servingIndex
            : 0;
    let lastStop = stops[snIndex - 1] || stops[0];

    let cur = serving || lastStop;
    let travelledToNextStop = percentTravelled(cur, nextStop);
    let realtime =
        (nextStop.arrival - cur.departure) * travelledToNextStop -
        (nextStop.arrival - vehicle.lastPing);
    let _delay =
        tripStart > 0 ? (vehicle.delay == null ? realtime : vehicle.delay) : 0;

    return {
        snIndex,
        servingIndex:
            tripStart > 0 ? (servingIndex === -1 ? null : servingIndex) : 0,
        nextStopIndex: tripStart > 0 ? nextStopIndex : 1,
        delay: Math.floor(_delay),
        travelledToNextStop,
        awayFromLine: properties.dist,
    };
};

function percentTravelled(stop1, stop2) {
    let res = stop1.metersToStop / (stop1.metersToStop - stop2.metersToStop);
    return res >= 1 || res === -Infinity ? 0 : 1 - res;
}
