const { Schema } = require("mongoose");

module.exports = {
    trip: new Schema({
        id: { type: String, required: true, unique: true },
        route: { type: String, required: true },
        headsign: { type: String, required: true },
        shape: { type: String, required: true },
        direction: { type: Number, required: true },
        brigade: { type: String, required: false },
        shortName: { type: String, required: false },
        brigade: { type: String, required: false },
        wheelchair: { type: Boolean, required: false },
        bikes: { type: Boolean, required: false },
        predict: { type: Boolean, required: false },
        start: { type: Number, required: true },
        end: { type: Number, required: true },
        stops: {
            type: [
                {
                    id: String,
                    name: String,
                    arrival: Number,
                    departure: Number,
                    sequence: Number,
                    on_request: Boolean,
                    location: [Number],
                    distance: Number,
                    index: Number,
                    platform: String,
                },
            ],
            required: true,
        },
    }),
    stop: new Schema({
        id: { type: String, required: true, unique: true },
        name: { type: String, required: true },
        code: { type: String, required: false },
        location: { type: [Number], required: true },
        type: { type: [Number], required: true },
        routes: { type: [String], required: true },
        deg: { type: [Number], required: true },
    }),
    route: new Schema({
        id: { type: String, required: true, unique: true },
        name: { type: String, required: true },
        longName: { type: String, required: false },
        type: { type: Number, required: true }
    }),
    shape: new Schema({
        id: { type: String, required: true, unique: true },
        points: { type: [[Number]], required: true },
    }),
    vehicle: new Schema({
        id: { type: String, required: true, unique: true },
        model: String,
        prodYear: String,
        registration: String,
        carrier: String,
        depot: String,
        features: { type: [String], required: true },
    }),
};
