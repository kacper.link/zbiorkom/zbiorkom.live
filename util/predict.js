const { along, lineString, nearestPointOnLine } = require("@turf/turf");
const calculateBearing = require("./calculateBearing");

module.exports = (trips) => {
    return trips.map((trip) => {
        let currentStop = trip.stops.find(
            (stop) => stop.arrival >= Date.now() && stop.departure <= Date.now()
        );
        if (currentStop)
            return returnLocation(
                currentStop.location,
                calculateBearing(
                    currentStop.location,
                    trip.shapes[currentStop.index + 1]
                ),
                trip
            );

        let previousStop = trip.stops.filter((stop) => stop.departure <= Date.now()).pop() || trip.stops[0];
        let nextStop = trip.stops.find((stop) => stop.arrival >= Date.now());
        let shape = lineString(trip.shapes);

        let currentLocation = along(
            shape,
            previousStop.distance + ((Date.now() - previousStop.departure) / (nextStop.arrival - previousStop.departure)) * (nextStop.distance - previousStop.distance),
            { units: "meters" }
        );

        let index = nearestPointOnLine(shape, currentLocation).properties.index;
        return returnLocation(
            currentLocation.geometry.coordinates,
            calculateBearing(
                currentLocation.geometry.coordinates,
                trip.shapes[index + 1 === trip.shapes.length ? index : index + 1]
            ),
            trip
        );
    });
};

const returnLocation = (location, bearing, trip) => ({
    route: trip.route,
    id: trip.id.replace(/[&\/?]/g, ""),
    location: location,
    brigade: trip.brigade,
    lastPing: Date.now(),
    bearing: bearing,
    trip: trip.id,
    isPredicted: true,
});
