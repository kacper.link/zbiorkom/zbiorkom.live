const { parse } = require("node-html-parser");

module.exports = async (url, retries = 10, html) => {
    let response = await fetch(url).catch(() => null);
    if (response?.status >= 200 && response?.status <= 399) return html ? parse(await response.text()) : response;
    if (retries < 1) return null;
    if (retries <= 5) console.log(`Retry ${url} > ${retries}`);
    return module.exports(url, retries - 1);
};
