const downloadGTFS = require("./downloadGTFS");
const calculateBearing = require("./calculateBearing");
const { lineString, nearestPointOnLine, point } = require("@turf/turf");
const {
    convertAgency,
    convertCalendar,
    convertFrequencies,
    convertRoutes,
    convertShapes,
    convertStopTimes,
    convertStops,
    convertTrips,
} = require("../loadGTFS");

module.exports = (link, id = "", nextDay = false) =>
    downloadGTFS(link).then((data) => {
        if (!data) throw new Error("No data");
        let agency = convertAgency(data["agency.txt"]);
        let calendar = convertCalendar(
            data["calendar.txt"],
            data["calendar_dates.txt"],
            nextDay
        );
        let frequencies = convertFrequencies(data["frequencies.txt"]);
        let routes = convertRoutes(data["routes.txt"], agency);
        let shapes = convertShapes(data["shapes.txt"]);
        let stopTimes = convertStopTimes(data["stop_times.txt"], nextDay);
        let stops = convertStops(data["stops.txt"]);
        let trips = convertTrips(data["trips.txt"], calendar);

        let _stops = {};

        trips = trips
            .map((trip) => {
                let route = routes.find((route) => route.id === trip.route);
                let stopT = stopTimes[trip.id];
                let isOneDirecition =
                    stopT[0].id === stopT[stopT.length - 1].id;
                let shape = shapes[trip.shape];
                let line = lineString(
                    shape.slice(0, isOneDirecition ? -1 : undefined)
                );

                let t = {
                    ...trip,
                    id: id + trip.id,
                    shape: id + trip.shape,
                    start: stopT[0].arrival - route.offset,
                    end: stopT[stopT.length - 1].departure - route.offset,
                    stops: stopT.map((stopTime, i) => {
                        let stop = stops.find(
                            (stop) => stop.id === stopTime.id
                        );
                        let np = nearestPointOnLine(
                            line,
                            point(stop.location),
                            { units: "meters" }
                        );

                        let _stop =
                            _stops[stop.pkp || stop.parent || stopTime.id];
                        _stops[stop.pkp || stop.parent || stopTime.id] = {
                            routes: [...(_stop?.routes || []), route.id],
                            deg: [
                                ...(_stop?.deg || []),
                                ...(stopT.length - 1 !== i &&
                                    np.properties.dist < 35 &&
                                    shape[np.properties.index + 1]
                                    ? [
                                        calculateBearing(
                                            np.geometry.coordinates,
                                            shape[np.properties.index + 1]
                                        ),
                                    ]
                                    : []),
                            ],
                        };

                        return {
                            ...stopTime,
                            id: `${id}${stop.pkp || stop.parent || stopTime.id
                                }`,
                            name: `${stop.name}${stop.code ? ` ${stop.code}` : ""
                                }`,
                            arrival: stopTime.arrival - route.offset,
                            departure: stopTime.departure - route.offset,
                            location:
                                np.properties.dist < 35
                                    ? np.geometry.coordinates
                                    : stop.location,
                            distance:
                                i === 0
                                    ? 0
                                    : Math.floor(np.properties.location),
                            index: i === 0 ? 0 : np.properties.index,
                            ...(stop.platform
                                ? { platform: stop.platform }
                                : {}),
                        };
                    }),
                };

                if (frequencies.find((freq) => freq.id === trip.id)) {
                    let frequency = frequencies.filter(
                        (freq) => freq.id === trip.id
                    );
                    let trips = [];
                    frequency.forEach((freq) => {
                        for (
                            let time = freq.startTime;
                            time < freq.endTime;
                            time += freq.headway
                        ) {
                            trips.push({
                                ...t,
                                id: `${trip.id}/${time}`,
                                start: time + t.start,
                                end: time + t.end,
                                stops: t.stops.map((stop) => ({
                                    ...stop,
                                    arrival: time + stop.arrival,
                                    departure: time + stop.departure,
                                })),
                            });
                        }
                    });
                    return trips;
                } else return t;
            })
            .flat();

        return {
            routes: routes.map((route) => ({
                id: route.id,
                name: route.name,
                longName: route.longName,
                type: route.type
            })),
            shapes: Object.keys(shapes)
                .filter((x) => trips.find((trip) => trip.shape === id + x))
                .map((shape) => ({
                    id: id + shape,
                    points: shapes[shape],
                })),
            stops: stops
                .filter((stop) => !stop.parent && _stops[stop.pkp || stop.id])
                .map((stop) => {
                    let _stop = _stops[stop.pkp || stop.id];
                    let route = [...new Set(_stop.routes)];
                    return {
                        id: id + (stop.pkp || stop.id),
                        name: stop.name,
                        code: stop.code,
                        location: stop.location,
                        type: [
                            ...new Set(
                                route.map(
                                    (ro) => routes.find((r) => r.id === ro).type
                                )
                            ),
                        ].sort((a, b) => a - b),
                        routes: route,
                        deg: removeSimilarNumbers([...new Set(_stop.deg)]),
                    };
                }),
            trips,
        };
    });

function removeSimilarNumbers(numbers, diff = 50) {
    let result = [];
    numbers.forEach((number) => {
        if (
            result.length === 0 ||
            Math.abs(number - result[result.length - 1]) > diff
        )
            result.push(number);
    });
    return result;
}
