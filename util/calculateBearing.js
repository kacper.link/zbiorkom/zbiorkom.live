const metersToPoint = require("./metersToPoint");

module.exports = (point1, point2, vehicleBearing) => {
    if (!point1 || !point2) return undefined;
    if (vehicleBearing && metersToPoint(point1, point2) < 0.02)
        return vehicleBearing;
    let deg =
        (Math.atan2(point2[1] - point1[1], point2[0] - point1[0]) * 180) /
        Math.PI;
    return Math.floor(deg < 0 ? deg + 360 : deg);
};
