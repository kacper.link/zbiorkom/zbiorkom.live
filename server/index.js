const validateCity = require("../util/validateCity");

module.exports = () => {
    fastify.addHook("preHandler", (req, res, done) => {
        if (req.params.city === "")
            return res.code(400).send({ error: "City not specified" });
        if (!req.params.city) return done();
        if (validateCity(req.params.city)) {
            req.city = req.params.city;
            return done();
        }
        res.code(404).send({ error: "City not found" });
    });

    fastify.addHook("onSend", (req, res, payload, done) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Content-Type", "application/json; charset=utf-8");
        done();
    });

    require("./staticData")(fastify);
    require("./stops")(fastify);
    require("./search")(fastify);
    require("./alerts")(fastify);
};
