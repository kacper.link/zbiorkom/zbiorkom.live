module.exports = () => {
    fastify.get("/:city/findStopGroup", async (req, res) => {
        let name = req.query.name;
        if (!name) return res.code(400).send({ error: "Missing stop name" });

        let stops = await global[req.city].stops
            .find(
                {
                    name: { $regex: new RegExp(name, "i") },
                },
                "name type"
            )
            .lean();

        return [...new Set(stops.map((stop) => stop.name))].map((name) => ({
            name,
            type: [
                ...new Set(
                    stops
                        .filter((stop) => stop.name === name)
                        .flatMap((stop) => stop.type)
                ),
            ].sort(),
        }));
    });

    fastify.get("/:city/getStopGroup", async (req, res) => {
        let name = req.query.name;
        if (!name) return res.code(400).send({ error: "Missing stop name" });

        let stops = await global[req.city].stops
            .find(
                {
                    name,
                },
                "id name code type"
            )
            .lean();

        return stops.map((stop) => ({
            id: stop.id,
            name: `${stop.name}${stop.code ? ` ${stop.code}` : ""}`,
            type: stop.type,
        }));
    });

    fastify.get("/:city/findVehicle", async (req, res) => {
        let id = req.query.id;
        let route = req.query.route;
        let brigade = req.query.brigade;

        let vehicles = global[req.city].positions.filter(
            (veh) =>
                veh.id === id ||
                (veh.route === route && veh.brigade === brigade)
        );
        return Promise.all(
            vehicles.map(async (vehicle) => {
                let trip = await global[req.city].trips
                    .findOne({ id: vehicle.trip }, "headsign")
                    .lean();
                let veh =
                    global[req.city].vehicles &&
                    (await global[req.city].vehicles
                        .findOne(
                            { id: `${vehicle.type}/${vehicle.id}` },
                            "model"
                        )
                        .lean());
                return {
                    id: vehicle.id,
                    type: vehicle.type,
                    route: vehicle.route,
                    brigade: vehicle.brigade,
                    headsign: trip.headsign,
                    model: veh?.model,
                };
            })
        );
    });
};
