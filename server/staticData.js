module.exports = () => {
    fastify.get("/:city/trip", async (req, res) => {
        if (!req.query.trip)
            return res.code(400).send({ error: "Trip not specified" });
        let trip = await global[req.city].trips
            .findOne({ id: req.query.trip })
            .lean();
        if (!trip) return res.code(404).send({ error: "Trip not found" });
        let shape = await global[req.city].shapes
            .findOne({ id: trip.shape })
            .lean();

        return {
            id: trip.id,
            headsign: trip.headsign,
            shortName: trip.shortName,
            shapes: shape.points,
            stops: trip.stops.map((stop) => ({
                arrival: stop.arrival,
                departure: stop.departure,
                distance: stop.distance,
                id: stop.id,
                index: stop.index,
                location: stop.location,
                name: stop.name,
                on_request: stop.on_request,
                platform: stop.platform,
                sequence: stop.sequence,
            })),
        };
    });

    fastify.get("/:city/routes", async (req) => {
        let routes = await global[req.city].routes
            .find({}, "id name type")
            .lean();

        return [...new Set(routes.map((r) => r.type))].map((type) => ({
            type,
            routes: routes
                .filter((r) => r.type === type)
                .map((r) => ({ id: r.id, name: r.name })),
        }));
    });

    fastify.get("/:city/vehicle", async (req, res) => {
        if (!req.query.vehicle)
            return res.code(400).send({ error: "Vehicle not specified" });
        let vehicle = await global[req.city].vehicles
            .findOne(
                { id: req.query.vehicle },
                "model prodYear registration carrier depot features"
            )
            .lean();
        if (!vehicle) return res.code(404).send({ error: "Vehicle not found" });
        return {
            model: vehicle.model,
            prodYear: vehicle.prodYear,
            registration: vehicle.registration,
            carrier: vehicle.carrier,
            depot: vehicle.depot,
            features: vehicle.features,
        };
    });

    fastify.get("/:city/positions", async (req) => global[req.city].positions);
};
