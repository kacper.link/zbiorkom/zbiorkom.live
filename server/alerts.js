module.exports = () => {
    fastify.get("/:city/alerts", async (req) => {
        let alerts = await global[req.city].alerts;
        return alerts.map((alert) => ({
            id: alert.id,
            title: alert.title,
            routes: alert.routes.length > 10 ? [...alert.routes.slice(0, 10), "..."] : alert.routes,
            start: alert.start,
            end: alert.end,
            impediment: alert.impediment
        }));
    });

    fastify.get("/:city/alert", async (req, res) => {
        let id = req.query.alert;
        if (!id) return res.code(400).send({ error: "Missing alert id" });
        let alert = await global[req.city].alerts.find((x) => x.id === id);
        if (!alert) return res.code(404).send({ error: "Alert not found" });
        return alert;
    });
};
