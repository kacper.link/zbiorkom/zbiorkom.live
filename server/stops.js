const realtime = require("../util/realtime");

module.exports = () => {
    fastify.get("/:city/stops", async (req) => {
        let stops = await global[req.city].stops
            .find({}, "id deg location type")
            .lean();
        return stops.map((stop) => ({
            id: stop.id,
            bearing: stop.deg,
            location: stop.location,
            type: stop.type,
        }));
    });

    fastify.get("/:city/stop", async (req, res) => {
        let id = req.query.stop;
        if (!id) return res.code(400).send({ error: "Missing stop id" });
        let stop = await global[req.city].stops
            .findOne({ id }, "id name location type routes deg")
            .lean();
        if (!stop) return res.code(404).send({ error: "Stop not found" });
        return {
            id: stop.id,
            name: `${stop.name}${stop.code ? ` ${stop.code}` : ""}`,
            location: stop.location,
            type: stop.type,
            routes: stop.routes,
            bearing: stop.deg,
        };
    });

    fastify.get("/:city/stopDepartures", async (req, res) => {
        let id = req.query.stop;
        if (!id) return res.code(400).send({ error: "Missing stop id" });
        let stop = await global[req.city].stops.findOne({ id }).lean();
        if (!stop) return res.code(404).send({ error: "Stop not found" });

        let trips = await global[req.city].trips
            .find({
                "stops.id": id,
            })
            .lean();

        let routes = await global[req.city].routes
            .find(
                {
                    id: { $in: stop.routes },
                },
                "id name type"
            )
            .lean();

        let d = await Promise.all(
            trips.map(async (trip) => {
                let vehicle = global[req.city].positions.find(
                    (veh) => veh.trip === trip.id
                );
                let isRealtime =
                    vehicle?.trip === trip.id &&
                    (!vehicle?.isPredicted || vehicle?.delay != null);

                let rt =
                    isRealtime &&
                    realtime({
                        vehicle,
                        trip: {
                            ...trip,
                            shapes: (
                                await global[req.city].shapes
                                    .findOne({ id: trip.shape }, "points")
                                    .lean()
                            ).points,
                        },
                    });
                let stop = trip.stops.find((stop) => stop.id === id);
                if (
                    isRealtime
                        ? rt.snIndex > stop.sequence || rt.awayFromLine > 250
                        : stop.departure < Date.now()
                )
                    return;
                let route = routes.find((x) => x.id === trip.route);

                return {
                    route: route?.name,
                    type: route?.type,
                    headsign: trip.headsign,
                    delay: rt?.delay,
                    status: isRealtime ? "REALTIME" : "SCHEDULED",
                    isLastStop: stop.sequence === trip.stops.length - 1,
                    realTime: stop.departure + (rt?.delay || 0),
                    scheduledTime: stop.departure,
                    platform: stop.platform,
                    trip: trip.id
                };
            })
        );

        return {
            name: stop.name,
            type: stop.type,
            code: stop.code,
            routes: routes
                .sort((a, b) =>
                    ("" + a.name).localeCompare(b.name, undefined, {
                        numeric: true,
                    })
                )
                .map(route => ([route.name, route.type])),
            departures: d
                .filter((x) => x)
                .sort((a, b) => a.realTime - b.realTime)
                .slice(0, 15),
        };
    });
};
