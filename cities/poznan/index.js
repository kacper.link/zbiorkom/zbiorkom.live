const mongoose = require("mongoose");
const loadGTFS = require("../../util/loadGTFS");
const retryFetch = require("../../util/retryFetch");
const positions = require("./positions");
const { schedule } = require("node-cron");
const { trip, stop, route, shape } = require("../../util/schema");

module.exports = () => {
    global.poznan = {
        trips: mongoose.model("poznan.trips", trip),
        stops: mongoose.model("poznan.stops", stop),
        routes: mongoose.model("poznan.routes", route),
        shapes: mongoose.model("poznan.shapes", shape),
        positions: [],
    };

    //load();

    schedule("50 2 * * *", load);
    schedule("*/20 * * * * *", async () => {
        let pos = await positions();
        poznan.positions = pos;
        fastify.io.to("poznan").emit("positions", pos);
    });

    async function load() {
        await mongoose.connection.db
            .dropCollection("poznan.trips")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("poznan.stops")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("poznan.routes")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("poznan.shapes")
            .catch(() => null);

        let url = await retryFetch(
            "https://www.ztm.poznan.pl/pl/dla-deweloperow/gtfsFiles",
            10,
            true
        ).then((document) =>
            Object.values(document.querySelectorAll("a"))
                .filter((x) => x.textContent === "Pobierz")
                .map((x) => x.rawAttributes.href)
                .find((x) => {
                    let fileName = x.split("=")[1].split("_")[0];
                    let rok = fileName.slice(0, 4);
                    let msc = fileName.slice(4, 6);
                    let day = fileName.slice(6, 8);
                    return (
                        new Date(
                            Number(rok),
                            Number(msc) - 1,
                            Number(day)
                        ).getTime() < Date.now()
                    );
                })
        );

        await loadGTFS(`https://www.ztm.poznan.pl/${url}`).then(async (d) => {
            await poznan.trips
                .insertMany(d.trips)
                .catch((err) => console.log(err));
            await poznan.stops
                .insertMany(
                    d.stops.map((stop) => ({
                        ...stop,
                        code: stop.code.slice(-2),
                    }))
                )
                .catch((err) => console.log(err));
            await poznan.routes
                .insertMany(
                    d.routes.map((route) => ({
                        ...route,
                        longName: route.longName.split("|")[0],
                    }))
                )
                .catch((err) => console.log(err));
            await poznan.shapes
                .insertMany(d.shapes)
                .catch((err) => console.log(err));
            console.log("[POZNAN] Imported");
        });
    }
};
