const rt = require("gtfs-rt-json-v2");
const calcBearing = require("../../util/calculateBearing");

module.exports = async () => {
    let data = await rt(
        "https://www.ztm.poznan.pl/pl/dla-deweloperow/getGtfsRtFile/?file=feeds.pb"
    )
        .then((x) => x.entity)
        .catch(() => []);

    let pos = await Promise.all(
        data
            .filter((x) => x.vehicle)
            .filter(
                (vehicle) =>
                    Date.now() - vehicle.vehicle.timestamp.low * 1000 < 180000
            )
            .map(async (entity) => {
                let delay =
                    data.find((x) => x.id === entity.id && x.tripUpdate)
                        ?.tripUpdate.stopTimeUpdate[0].arrival.delay * 1000 ||
                    undefined;
                let type = (
                    await poznan.routes
                        .findOne({ id: entity.vehicle.trip.routeId }, "type")
                        .lean()
                )?.type;
                type = type == null ? 3 : type;

                let previousLocation = poznan.positions.find(
                    (x) => x.id === entity.id
                );

                return {
                    route: entity.vehicle.vehicle.label.split("/")[0],
                    id: entity.id,
                    type,
                    location: [
                        entity.vehicle.position.latitude,
                        entity.vehicle.position.longitude,
                    ],
                    brigade: entity.vehicle.vehicle.label.split("/")[1],
                    lastPing: entity.vehicle.timestamp.low * 1000,
                    bearing:
                        Math.floor(
                            previousLocation?.lastPing ===
                                entity.vehicle.timestamp.low * 1000
                                ? previousLocation.bearing
                                : calcBearing(
                                      previousLocation?.location,
                                      [
                                          entity.vehicle.position.latitude,
                                          entity.vehicle.position.longitude,
                                      ],
                                      previousLocation?.bearing
                                  )
                        ) || undefined,
                    trip: entity.vehicle.trip.tripId,
                    delay,
                };
            })
    );

    return pos.length ? pos : poznan.positions;
};
