const mongoose = require("mongoose");
const loadGTFS = require("../../util/loadGTFS");
const loadAlerts = require("../../util/loadAlerts");
const retryFetch = require("../../util/retryFetch");
const positions = require("./positions");
const vehicles = require("./vehicles");
const { writeFileSync } = require("fs");
const { schedule } = require("node-cron");
const { trip, stop, route, shape, vehicle } = require("../../util/schema");

module.exports = () => {
    global.warsaw = {
        trips: mongoose.model("warsaw.trips", trip),
        stops: mongoose.model("warsaw.stops", stop),
        routes: mongoose.model("warsaw.routes", route),
        shapes: mongoose.model("warsaw.shapes", shape),
        vehicles: mongoose.model("warsaw.vehicles", vehicle),
        alerts: [],
        positions: [],
    };

    //load();
    //loadVehicles();

    schedule("30 2 * * *", load);
    schedule("0 3 */10 * *", loadVehicles);
    schedule("*/15 * * * * *", async () => {
        let pos = await positions();
        warsaw.positions = pos;
        fastify.io.to("warsaw").emit("positions", pos);
    });
    schedule("*/5 * * * *", async () => {
        let ale = await loadAlerts("https://mkuran.pl/gtfs/warsaw/alerts.pb");
        warsaw.alerts = ale;
    });

    async function load() {
        await mongoose.connection.db.dropCollection("warsaw.trips").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.stops").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.routes").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.shapes").catch(() => null);
        await mongoose.connection.db.dropCollection("warsaw.brigades").catch(() => null);

        let url = await retryFetch("https://mkuran.pl/gtfs/warsaw/feeds/modtimes.json").then((r) => r.json()).then((r) => Object.keys(r).map((x) => x.slice(2))).then((data) => {
            let today = new Date()
                .toLocaleDateString("en-gb")
                .split("/")
                .reverse()
                .join("")
                .slice(2);
            return data
                .filter((x) => x <= today)
                .sort()
                .pop();
        });

        let stops = [];

        await loadGTFS(`https://mkuran.pl/gtfs/warsaw/feeds/RA${url}.zip`).then(async (d) => {
            let brigades = await retryFetch("https://mkuran.pl/gtfs/warsaw/brigades.json").then((r) => r.json()).catch(() => { });
            let _brigades = {};

            await warsaw.trips
                .insertMany(
                    d.trips.map((trip) => {
                        let lineBrigade = brigades[trip.route];
                        let brigade = lineBrigade
                            ? Object.keys(lineBrigade).find((brigade) =>
                                lineBrigade[brigade].find(
                                    (tw) =>
                                        tw.trip_id
                                            .split("/")
                                            .slice(1)
                                            .join("/") === trip.id
                                )
                            )
                            : trip.brigade;

                        if (brigade) {
                            let firstStop = trip.stops[0];
                            let lastStop = trip.stops[trip.stops.length - 1];
                            if (!_brigades[`${trip.route}/${brigade}`])
                                _brigades[`${trip.route}/${brigade}`] = [
                                    {
                                        trip: trip.id,
                                        location: lastStop.location,
                                        headsign: trip.headsign,
                                        firstStop: firstStop.id,
                                        start: firstStop.arrival,
                                        end: lastStop.departure,
                                    },
                                ];
                            else
                                _brigades[`${trip.route}/${brigade}`].push({
                                    trip: trip.id,
                                    location: lastStop.location,
                                    headsign: trip.headsign,
                                    firstStop: firstStop.id,
                                    start: firstStop.arrival,
                                    end: lastStop.departure,
                                });
                            _brigades[`${trip.route}/${brigade}`] = _brigades[
                                `${trip.route}/${brigade}`
                            ].sort((a, b) => a.start - b.start);
                        }

                        return {
                            ...trip,
                            predict:
                                trip.route.includes("S") ||
                                trip.route === "36" ||
                                trip.route === "100",
                            brigade,
                        };
                    })
                )
                .catch((err) => console.log(err));
            stops = d.stops.map((stop) => {
                let name = stop.name.split(" ");
                return {
                    ...stop,
                    name: !isNaN(name[name.length - 1])
                        ? name.slice(0, -1).join(" ")
                        : stop.name,
                    code: !isNaN(name[name.length - 1])
                        ? name[name.length - 1]
                        : undefined,
                };
            });
            await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
            await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
            writeFileSync("./cities/warsaw/brigades.json", JSON.stringify(_brigades, null, 4));

            console.log("[WARSAW] Imported bus, tram and SKM train");
            return d.stops.filter((stop) => stop.type[0] === "2");
        });
        await loadGTFS("https://mkuran.pl/gtfs/kolejemazowieckie.zip").then(
            async (d) => {
                stops = [...stops, ...d.stops];
                await warsaw.routes
                    .insertMany(d.routes)
                    .catch((err) => console.log(err));
                await warsaw.shapes
                    .insertMany(d.shapes)
                    .catch((err) => console.log(err));
                await warsaw.trips
                    .insertMany(
                        d.trips.map((trip) => ({
                            ...trip,
                            predict: true,
                        }))
                    )
                    .catch((err) => console.log(err));
                console.log("[WARSAW] Imported KM train and ZKA bus");
            }
        );
        await loadGTFS("https://mkuran.pl/gtfs/warsaw/metro.zip").then(
            async (d) => {
                await warsaw.stops.insertMany(d.stops).catch((err) => console.log(err));
                await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
                await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
                await warsaw.trips.insertMany(d.trips.map((trip) => ({
                    ...trip,
                    predict: true,
                }))).catch((err) => console.log(err));
                console.log("[WARSAW] Imported subway");
            }
        );
        await loadGTFS("https://mkuran.pl/gtfs/wkd.zip", "d").then(
            async (d) => {
                await warsaw.stops.insertMany(d.stops).catch((err) => console.log(err));
                await warsaw.routes.insertMany(d.routes).catch((err) => console.log(err));
                await warsaw.shapes.insertMany(d.shapes).catch((err) => console.log(err));
                await warsaw.trips.insertMany(d.trips.map((trip) => ({
                    ...trip,
                    predict: true,
                }))).catch((err) => console.log(err));
                console.log("[WARSAW] Imported WKD train");
            }
        );
        await loadGTFS(
            "https://github.com/absrdld/kgd-gtfs/archive/refs/heads/main.zip",
            "kgd"
        ).then(async (d) => {
            stops = [...stops, ...d.stops
                .map((stop) => {
                    let name = stop.name.split(" ");
                    return {
                        ...stop,
                        id: stop.id.replace("kgd", ""),
                        name: !isNaN(name[name.length - 1])
                            ? name.slice(0, -1).join(" ")
                            : stop.name,
                        code: !isNaN(name[name.length - 1])
                            ? name[name.length - 1]
                            : undefined,
                    };
                })];
            await warsaw.routes
                .insertMany(d.routes)
                .catch((err) => console.log(err));
            await warsaw.shapes
                .insertMany(d.shapes)
                .catch((err) => console.log(err));
            await warsaw.trips
                .insertMany(
                    d.trips.map((trip) => ({
                        ...trip,
                        predict: true,
                    }))
                )
                .catch((err) => console.log(err));
            console.log("[WARSAW] Imported KGD bus");
        });
        await warsaw.stops.insertMany(stops.filter((value, index, self) => index === self.findIndex((t) => t.id === value.id)).map(stop => {
            let s = stops.filter(s => s.id === stop.id);
            return {
                ...stop,
                routes: [...new Set(s.map(s => s.routes).flat())],
                deg: [...new Set(s.map(s => s.deg).flat())],
            };
        })).catch((err) => console.log(err));
    }

    async function loadVehicles() {
        let now = Date.now();
        await mongoose.connection.db
            .dropCollection("warsaw.vehicles")
            .catch(() => null);
        let veh = await vehicles();
        warsaw.vehicles.insertMany(veh);
        console.log(
            `[WARSAW] Loaded ${veh.length} vehicles in ${Date.now() - now}ms`
        );
    }
};
