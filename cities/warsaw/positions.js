const calcBearing = require("../../util/calculateBearing");
const metersToPoint = require("../../util/metersToPoint");
const predict = require("../../util/predict");
const { readFileSync } = require("fs");

module.exports = async () => {
    let brigades = JSON.parse(
        readFileSync("./cities/warsaw/brigades.json", "utf-8")
    );

    let [bus, tram] = await Promise.all([
        getType("1", 4),
        getType("2", 4)
    ]);

    let pos = await Promise.all(
        [...bus, ...tram]
            .filter(
                (vehicle) =>
                    Date.now() - new Date(`${vehicle.Time}+0200`) < 180000
            )
            .map(async (vehicle) => {
                let type = (
                    await warsaw.routes
                        .findOne({ id: vehicle.Lines }, "type")
                        .lean()
                )?.type;
                type = type == null ? 3 : type;
                
                let previousLocation = warsaw.positions.find(
                    (x) => x.id === vehicle.VehicleNumber && x.type === type
                );
                let lastPing = new Date(`${vehicle.Time}+0200`).getTime();

                return {
                    route: vehicle.Lines,
                    id: vehicle.VehicleNumber,
                    type,
                    location: [vehicle.Lat, vehicle.Lon],
                    brigade: vehicle.Brigade,
                    lastPing,
                    bearing:
                        lastPing === previousLocation?.lastPing
                            ? previousLocation.bearing
                            : calcBearing(
                                  previousLocation?.location,
                                  [vehicle.Lat, vehicle.Lon],
                                  previousLocation?.bearing
                              ) || undefined,
                    trip: matchTrip(
                        vehicle,
                        previousLocation?.trip,
                        brigades[`${vehicle.Lines}/${vehicle.Brigade}`]
                    ),
                };
            })
    );

    return pos.length
        ? [
              ...pos,
              ...(await Promise.all(
                  predict(
                      await Promise.all(
                          (
                              await warsaw.trips
                                  .find({
                                      start: { $lte: Date.now() },
                                      end: { $gte: Date.now() },
                                      predict: true,
                                  })
                                  .lean()
                          ).map(async (trip) => ({
                              ...trip,
                              shapes: (
                                  await warsaw.shapes
                                      .findOne({ id: trip.shape }, "points")
                                      .lean()
                              ).points,
                          }))
                      )
                  ).map(async (vehicle) => ({
                      ...vehicle,
                      type:
                          (
                              await warsaw.routes
                                  .findOne({ id: vehicle.route }, "type")
                                  .lean()
                          )?.type || 2,
                  }))
              )),
          ]
        : warsaw.positions;
};

function matchTrip(vehicle, previousTrip, trips) {
    if (!trips?.length) return;
    if (previousTrip) {
        let currentTrip = trips.find((x) => x.trip === previousTrip);
        if (!currentTrip) return;
        let secondsToEnd = (currentTrip.end - Date.now()) / 1000;
        if (
            (metersToPoint([vehicle.Lat, vehicle.Lon], currentTrip.location) <
                0.3 &&
                secondsToEnd < 300) ||
            secondsToEnd < -3000
        )
            return trips[trips.findIndex((x) => x.trip === previousTrip) + 1]
                ?.trip;
        else return previousTrip;
    } else {
        let current = trips.find(
            (x) => x.start < Date.now() && x.end > Date.now()
        );
        if (current) return current.trip;

        if (
            trips[0].start - Date.now() < 900000 &&
            trips[0].start - Date.now() > 0
        )
            return trips[0].trip;
        if (
            trips[trips.length - 1].end - Date.now() < 900000 &&
            trips[trips.length - 1].end - Date.now() > 0
        )
            return trips[trips.length - 1].trip;

        let petla = trips.find(
            (x, i) =>
                x.start > Date.now() &&
                trips[i - 1] &&
                trips[i - 1].end < Date.now()
        );
        if (petla) return petla.trip;
    }
}

async function getType(type, retries) {
    let data = await fetch(`https://api.um.warszawa.pl/api/action/busestrams_get/?resource_id=f2e5503e927d-4ad3-9500-4ab9e55deb59&apikey=aba985e1-23f8-4ea6-b7e6-b1d053fabbc0&type=${type}`)
        .then((r) => r.json())
        .then((r) => r.result)
        .catch(() => null);
    if (!data && retries > 0) return getType(type, retries - 1);
    return data;
}