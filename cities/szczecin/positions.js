const calcBearing = require("../../util/calculateBearing");

module.exports = async () => {
    let data = await fetch("https://www.zditm.szczecin.pl/json/pojazdy.inc.php")
        .then((res) => res.json())
        .catch(() => []);

    let pos = data.filter((vehicle) => Date.now() - new Date(`${new Date().toDateString()} ${vehicle.aktualizacja}+0200`) < 180000).map((vehicle) => {
        let type = vehicle.linia <= 49 ? 0 : 3;
        let previousLocation = szczecin.positions.find(
            (x) => x.id === vehicle.pojazd && x.type === type
        );
        let lastPing = new Date(`${new Date().toDateString()} ${vehicle.aktualizacja}+0200`).getTime();

        return {
            route: vehicle.linia,
            id: vehicle.pojazd,
            type,
            location: [Number(vehicle.lat), Number(vehicle.lon)],
            brigade: vehicle.brygada,
            lastPing,
            bearing:
                lastPing === previousLocation?.lastPing
                    ? previousLocation.bearing
                    : calcBearing(
                        previousLocation?.location,
                        [Number(vehicle.lat), Number(vehicle.lon)],
                        previousLocation?.bearing
                    ) || undefined,
            trip: `${vehicle.brygada}_${vehicle.trasa}`,
            delay: Number(
                vehicle.punktualnosc2
                    .replace("&minus;", "+")
                    .replace("&plus;", "-")
            ),
        };
    });

    return pos.length ? pos : szczecin.positions;
};
