const rt = require("gtfs-rt-json-v2");
const calcBearing = require("../../util/calculateBearing");

module.exports = async () => {
    let [positions, tripUpdates] = await Promise.all([
        rt(
            "https://romamobilita.it/sites/default/files/rome_rtgtfs_vehicle_positions_feed.pb"
        )
            .then((x) => x.entity)
            .catch(() => []),
        rt(
            "https://romamobilita.it/sites/default/files/rome_rtgtfs_trip_updates_feed.pb"
        )
            .then((x) => x.entity)
            .catch(() => []),
    ]);

    let pos = await Promise.all(
        positions
            .filter((x) => x.vehicle)
            .filter(
                (vehicle) =>
                    Date.now() - vehicle.vehicle.timestamp.low * 1000 < 180000
            )
            .map(async (entity) => {
                let type = (
                    await rome.routes
                        .findOne({ id: entity.vehicle.trip.routeId }, "type")
                        .lean()
                )?.type;
                type = type == null ? 3 : type;

                let previousLocation = rome.positions.find(
                    (x) => x.id === entity.id && x.type === type
                );
                let delay =
                    tripUpdates.find((x) => x.id === entity.id)?.tripUpdate
                        .delay * 1000 || undefined;

                return {
                    route: entity.vehicle.trip.routeId,
                    id: entity.id,
                    type,
                    location: [
                        entity.vehicle.position.latitude,
                        entity.vehicle.position.longitude,
                    ],
                    lastPing: entity.vehicle.timestamp.low * 1000,
                    bearing:
                        Math.floor(
                            previousLocation?.lastPing ===
                                entity.vehicle.timestamp.low * 1000
                                ? previousLocation.bearing
                                : calcBearing(
                                      previousLocation?.location,
                                      [
                                          entity.vehicle.position.latitude,
                                          entity.vehicle.position.longitude,
                                      ],
                                      previousLocation?.bearing
                                  )
                        ) || undefined,
                    trip: entity.vehicle.trip.tripId,
                    delay,
                };
            })
    );

    return pos.length ? pos : rome.positions;
};
