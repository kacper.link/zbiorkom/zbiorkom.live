const mongoose = require("mongoose");
const loadGTFS = require("../../util/loadGTFS");
const loadAlerts = require("../../util/loadAlerts");
const positions = require("./positions");
const { schedule } = require("node-cron");
const { trip, stop, route, shape } = require("../../util/schema");

module.exports = () => {
    global.rome = {
        trips: mongoose.model("rome.trips", trip),
        stops: mongoose.model("rome.stops", stop),
        routes: mongoose.model("rome.routes", route),
        shapes: mongoose.model("rome.shapes", shape),
        positions: [],
        alerts: [],
    };

    //load();

    schedule("0 3 * * *", load);
    schedule("*/30 * * * * *", async () => {
        let pos = await positions();
        rome.positions = pos;
        fastify.io.to("rome").emit("positions", pos);
    });
    schedule("*/5 * * * *", async () => {
        let ale = await loadAlerts(
            "https://romamobilita.it/sites/default/files/rome_rtgtfs_service_alerts_feed.pb"
        );
        rome.alerts = ale;
    });

    async function load() {
        await mongoose.connection.db
            .dropCollection("rome.trips")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("rome.stops")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("rome.routes")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("rome.shapes")
            .catch(() => null);

        await loadGTFS(
            "https://storage.googleapis.com/storage/v1/b/mdb-latest/o/it-lazio-roma-servizi-per-la-mobilita-gtfs-1294.zip?alt=media"
        ).then(async (d) => {
            await rome.trips
                .insertMany(d.trips)
                .catch((err) => console.log(err));
            await rome.stops
                .insertMany(d.stops)
                .catch((err) => console.log(err));
            await rome.routes
                .insertMany(d.routes)
                .catch((err) => console.log(err));
            await rome.shapes
                .insertMany(d.shapes)
                .catch((err) => console.log(err));
            console.log("[rome] Imported");
        });
    }
};
