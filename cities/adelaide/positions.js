const rt = require("gtfs-rt-json-v2");

module.exports = async () => {
    let positions = await rt(
        "https://gtfs.adelaidemetro.com.au/v1/realtime/vehicle_positions"
    )
        .then((x) => x.entity)
        .catch(() => []);

    let pos = await Promise.all(
        positions
            .filter((x) => x.vehicle)
            .filter(
                (vehicle) =>
                    Date.now() - vehicle.vehicle.timestamp.low * 1000 < 180000
            )
            .map(async (entity) => {
                let type = (
                    await adelaide.routes
                        .findOne({ id: entity.vehicle.trip.routeId }, "type")
                        .lean()
                )?.type;
                type = type == null ? 3 : type;

                return {
                    route: entity.vehicle.trip.routeId,
                    id: entity.id,
                    type,
                    location: [
                        entity.vehicle.position.latitude,
                        entity.vehicle.position.longitude,
                    ],
                    lastPing: entity.vehicle.timestamp.low * 1000,
                    bearing: Math.floor(entity.vehicle.position.bearing),
                    trip: entity.vehicle.trip.tripId,
                };
            })
    );

    return pos.length ? pos : adelaide.positions;
};
