const mongoose = require("mongoose");
const loadGTFS = require("../../util/loadGTFS");
const loadAlerts = require("../../util/loadAlerts");
const positions = require("./positions");
const { schedule } = require("node-cron");
const { trip, stop, route, shape } = require("../../util/schema");

module.exports = () => {
    global.adelaide = {
        trips: mongoose.model("adelaide.trips", trip),
        stops: mongoose.model("adelaide.stops", stop),
        routes: mongoose.model("adelaide.routes", route),
        shapes: mongoose.model("adelaide.shapes", shape),
        positions: [],
        alerts: [],
    };

    //load();

    schedule("0 21 * * *", load);
    schedule("*/30 * * * * *", async () => {
        let pos = await positions();
        adelaide.positions = pos;
        fastify.io.to("adelaide").emit("positions", pos);
    });
    schedule("*/5 * * * *", async () => {
        let ale = await loadAlerts(
            "https://gtfs.adelaidemetro.com.au/v1/realtime/service_alerts"
        );
        adelaide.alerts = ale;
    });

    async function load() {
        await mongoose.connection.db
            .dropCollection("adelaide.trips")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("adelaide.stops")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("adelaide.routes")
            .catch(() => null);
        await mongoose.connection.db
            .dropCollection("adelaide.shapes")
            .catch(() => null);

        await loadGTFS(
            "https://storage.googleapis.com/storage/v1/b/mdb-latest/o/au-south-australia-adelaide-metro-gtfs-660.zip?alt=media",
            "",
            true
        ).then(async (d) => {
            await adelaide.trips
                .insertMany(d.trips)
                .catch((err) => console.log(err));
            await adelaide.stops
                .insertMany(d.stops)
                .catch((err) => console.log(err));
            await adelaide.routes
                .insertMany(d.routes)
                .catch((err) => console.log(err));
            await adelaide.shapes
                .insertMany(d.shapes)
                .catch((err) => console.log(err));
            console.log("[ADELAIDE] Imported");
        });
    }
};
